namespace E_learningEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EventUser : DbMigration
    {
        public override void Up()
        {
            RenameColumn("dbo.Events", "User_id", "user_id");
        }

        public override void Down()
        {
            RenameColumn("dbo.Events", "user_id", "User_id");
        }
    }
}
