namespace E_learningEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PageTitle : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pages", "Titlu", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Pages", "Titlu");
        }
    }
}
