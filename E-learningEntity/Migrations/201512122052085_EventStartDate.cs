namespace E_learningEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EventStartDate : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Events", new[] { "User_Id" });
            CreateIndex("dbo.Events", "user_id");
            DropColumn("dbo.Events", "Start_date");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Events", "Start_date", c => c.DateTime(nullable: false));
            DropIndex("dbo.Events", new[] { "user_id" });
            CreateIndex("dbo.Events", "User_Id");
        }
    }
}
