namespace E_learningEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NullUser : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "Registration_number", c => c.Int());
            AlterColumn("dbo.Users", "Suspended", c => c.Boolean());
            AlterColumn("dbo.Users", "Blocked", c => c.Boolean());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Blocked", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Users", "Suspended", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Users", "Registration_number", c => c.Int(nullable: false));
        }
    }
}
