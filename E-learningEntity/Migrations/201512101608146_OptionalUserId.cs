namespace E_learningEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OptionalUserId : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.UserConversations", new[] { "user_id" });
            DropIndex("dbo.UserAnswers", new[] { "user_Id" });
            AlterColumn("dbo.UserConversations", "user_id", c => c.Int());
            AlterColumn("dbo.UserAnswers", "user_Id", c => c.Int());
            CreateIndex("dbo.UserConversations", "user_id");
            CreateIndex("dbo.UserAnswers", "user_Id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.UserAnswers", new[] { "user_Id" });
            DropIndex("dbo.UserConversations", new[] { "user_id" });
            AlterColumn("dbo.UserAnswers", "user_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.UserConversations", "user_id", c => c.Int(nullable: false));
            CreateIndex("dbo.UserAnswers", "user_Id");
            CreateIndex("dbo.UserConversations", "user_id");
        }
    }
}
