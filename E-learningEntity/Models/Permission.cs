﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_learningEntity.Models
{
    public class Permission
    {        
        public int Id { get; set; }
        [Required]
        public string Role { get; set; }

        [ForeignKey("User")]
        public int? user_id { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("Course")]
        public int? course_id { get; set; }
        public virtual Course Course { get; set; }
        
    }
}