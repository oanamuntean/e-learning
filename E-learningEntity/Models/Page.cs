﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_learningEntity.Models
{
    public class Page
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Title { get; set; }

        [ForeignKey("Group")]
        public int group_id { get; set; }
        public virtual Group Group { get; set; }

        [ForeignKey("User")]
        public int? user_id { get; set; }
        public virtual User User { get; set; }
        
    }
}