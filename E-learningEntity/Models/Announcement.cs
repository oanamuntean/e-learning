﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_learningEntity.Models
{
    public class Announcement
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public string Message { get; set; }

        [ForeignKey("Course")]
        public int? course_id { get; set; }
        public virtual Course Course { get; set; }

        [ForeignKey("Group")]
        public int? group_id { get; set; }
        public virtual Group Group { get; set; }
    }
}