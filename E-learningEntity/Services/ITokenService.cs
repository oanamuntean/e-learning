﻿using E_learningEntity.Models;
using System;


namespace E_learningEntity.Services
{
    public interface ITokenServices
    {
        #region Interface member methods.
        /// <summary>
        ///  Function to generate unique token with expiry against the provided userId.
        ///  Also add a record in database for generated token.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Token GenerateToken(int userId);

        /// <summary>
        /// Function to validate token againt expiry and existance in database.
        /// </summary>
        /// <param name="tokenId"></param>
        /// <returns></returns>
        bool ValidateToken(string tokenId);

        /// <summary>
        /// Method to kill the provided token id.
        /// </summary>
        /// <param name="tokenId"></param>
        bool Kill(string tokenId);

        /// <summary>
        /// Delete tokens for the specific deleted user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        bool DeleteByUserId(int userId);

        bool ValidateTokenAdmin(String tokenId);
        bool ValidateTokenTeacherForCourse(string tokenId, int course_id);
        bool ValidateTokenAssistantForCourse(string tokenId, int course_id);
        bool ValidateTokenStudentForCourse(string tokenId, int course_id);
        bool ValidateTokenTeacher(string tokenId);
        bool ValidateTokenStudent(string tokenId);
        bool ValidateTokenAssistant(string tokenId);
        bool ValidateCoursePermissions(string tokenId, int course_id);
        int GetUserIdByToken(string tokenId);
        #endregion
    }
}