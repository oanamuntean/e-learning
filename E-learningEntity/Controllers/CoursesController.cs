﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using E_learningEntity.Models;
using E_learningEntity.Services;
using E_learningEntity.ActionFilters;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace E_learningEntity.Controllers
{
    [RoutePrefix("courses")]
    public class CoursesController : ApiController
    {
        private E_learningEntityContext db = new E_learningEntityContext();
        UnitOfWork _unitOfWork = new UnitOfWork();

        /*
            * GetCoursesWithPermissions
            *
            * Get all courses with permissions
            *
            * @return (ICollection<JObject>)
            */
        [Route("permissions")]
        [HttpGet]
        [AdminAuthorizationRequired]
        public ICollection<JObject> GetCoursesWithPermissions()
        {
            List<JObject> list = new List<JObject>();
            foreach(Course curs in db.Courses)
            {
                JObject o = new JObject();
                o["Id"] = curs.Id;
                o["Name"] = curs.Name;
                o["Description"] = curs.Description;
                o["Department"] = curs.Department;
                o["Start_date"] = curs.Start_date;
                o["End_date"] = curs.End_date;
                JArray array = new JArray();
                foreach (Permission permission in _unitOfWork.PermissionRepository.GetManyQueryable(perm => perm.course_id == curs.Id && (perm.Role.Equals("Profesor") || perm.Role.Equals("Asistent")))){ 
                    JObject item = new JObject();
                    item["Role"] = permission.Role;
                    item["Id"] = permission.Id;
                    item["Course"] = permission.Course.Name;
                    item["Username"] = permission.User.Username;
                    array.Add(item);
                }
                o["Permissions"] = array;
                list.Add(o);
            }
            return list;
        }


        [Route("")]
        [HttpGet]
        [AdminAuthorizationRequired]
        public ICollection<JObject> GetCourses()
        {
            List<JObject> list = new List<JObject>();
            foreach (Course curs in db.Courses)
            {
                JObject o = new JObject();
                o["Id"] = curs.Id;
                o["Name"] = curs.Name;
                o["Description"] = curs.Description;
                o["Department"] = curs.Department;
                list.Add(o);
            }
            return list;

        }

        /*
             * GetOtherCourse
             *
             * Get course you are not enroll
             *
             * @return (List<JObject)
             */
        [Route("others")]
        [HttpGet]
        [AuthorizationRequired]
        public List<JObject> GetOtherCourses()
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            IQueryable<Permission> myPermissions = _unitOfWork.PermissionRepository.GetManyQueryable(perm => perm.user_id == userId);
            List<int> myCourses = new List<int>();

            foreach (Permission perm in myPermissions)
            {
                if (!myCourses.Contains((int)perm.course_id))
                    myCourses.Add((int)perm.course_id);
            }

            IQueryable<Course> allCourses = _unitOfWork.CourseRepository.GetAll();
            List<JObject> others = new List<JObject>();
            foreach (Course curs in allCourses)
            {
                //returneaza cursurile care nu s-au terminat deja pana acum
                if (!myCourses.Contains(curs.Id) && curs.End_date > DateTime.Today)
                {
                    Permission permission = _unitOfWork.PermissionRepository.Get(perm => perm.course_id == curs.Id && perm.Role.Equals("Profesor"));
                    JObject o = new JObject();
                    o["Id"] = curs.Id;
                    o["Name"] = curs.Name;
                    o["Description"] = curs.Description;
                    o["Department"] = curs.Department;
                    o["Start_date"] = curs.Start_date;
                    o["End_date"] = curs.End_date;
                    if (permission != null)
                        o["Profesor"] = permission.User.Last_name + " " +permission.User.First_name;
                    else
                        o["Profesor"] = "";
                    others.Add(o);
                }
            }
            return others;
        }

        /*
            * GetMyCourse
            *
            * Get my course to which i am enrolled
            *
            * @return (List<JObject>)
            */
        [Route("mycourses")]
        [AuthorizationRequired]
        public List<JObject> GetMyCourses()
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            IQueryable<Permission> myPermissions = _unitOfWork.PermissionRepository.GetManyQueryable(perm => perm.user_id == userId);
            List<Course> myCourses = new List<Course>();
            List<JObject> cursuri = new List<JObject>();
            foreach (Permission perm in myPermissions)
            {
                if (!myCourses.Contains(perm.Course))
                    myCourses.Add(perm.Course);
            }
            foreach (Course curs in myCourses)
            {
                Permission permission = _unitOfWork.PermissionRepository.Get(perm => perm.course_id == curs.Id && perm.Role.Equals("Profesor"));
                JObject o = new JObject();
                o["Id"] = curs.Id;
                o["Name"] = curs.Name;
                o["Description"] = curs.Description;
                o["Department"] = curs.Department;
                o["Start_date"] = curs.Start_date;
                o["End_date"] = curs.End_date;
                if (permission != null)
                    o["Profesor"] = permission.User.Last_name + " " +permission.User.First_name;
                else
                    o["Profesor"] = "";
                cursuri.Add(o);
            }
            return cursuri;
        }

        /*
            * GetPermissionForCourse
            *
            * Get my role for a course
            *
            * @courseId (int) course id
            * @return (JObject)
            */
        [Route("mycourses/{courseId:int}/permission")]
        [HttpGet]
        [AuthorizationRequired]
        public JObject GetPermissionForCourse(int courseId)
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            Permission myPermission = _unitOfWork.PermissionRepository.GetSingle(perm => perm.user_id == userId && perm.course_id == courseId);
            JObject o = new JObject();
            o["Id"] =myPermission.Id;
            o["Role"] = myPermission.Role;
            o["user_id"] = myPermission.user_id;
            o["course_id"] = myPermission.course_id;
            return o;
        }

        /*
            * GetCourse
            *
            * Get course by id
            *
            * @courseId (int) course id
            * @return (JObject)
            */
        [ResponseType(typeof(JObject))]
        [Route("{courseId:int}", Name = "GetCourseById")]
        [HttpGet]
        [CoursePermissionRequired]
        public async Task<IHttpActionResult> GetCourse(int courseId)
        {
            Course course = await db.Courses.FindAsync(courseId);
            if (course == null)
            {
                return NotFound();
            }
            JObject o = new JObject();
            o["Id"] = course.Id;
            o["Name"] = course.Name;
            o["Description"] = course.Description;
            o["Department"] = course.Department;
            o["Start_date"] = course.Start_date;
            o["PondereTests"] = course.PondereTests;
            o["PondereHomeworks"] = course.PondereHomeworks;
            o["PondereDiscussions"] = course.PondereDiscussions;
            o["End_date"] = course.End_date;
            return Ok(o);
        }


        [Route("{courseId:int}")]
        [ResponseType(typeof(void))]
        [CourseTeacherAuthorizationRequired]
        [HttpPut]
        public async Task<IHttpActionResult> PutCourse(int courseId, Course course)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (courseId != course.Id)
            {
                return BadRequest();
            }

            db.Entry(course).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CourseExists(courseId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Courses
        [Route("")]
        [HttpPost]
        [ResponseType(typeof(Course))]
        [TeacherAuthorizationRequired]
        public async Task<IHttpActionResult> PostCourse(Course course)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            
            db.Courses.Add(course);
            await db.SaveChangesAsync();
            return Ok(course);

        }

        // DELETE: api/Courses/5
        [Route("{courseId:int}")]
        [HttpDelete]
        [ResponseType(typeof(Course))]
        [CourseTeacherAuthorizationRequired]
        public async Task<IHttpActionResult> DeleteCourse(int courseId)
        {
            Course course = await db.Courses.FindAsync(courseId);
            if (course == null)
            {
                return NotFound();
            }

            db.Courses.Remove(course);
            await db.SaveChangesAsync();

            return Ok(course);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CourseExists(int id)
        {
            return db.Courses.Count(e => e.Id == id) > 0;
        }


        //get all discussions for a course
        [Route("{courseId:int}/discussions")]
        [HttpGet]
        [CoursePermissionRequired]
        public ICollection<JObject> FindDiscussionByCourse(int courseId)
        {
            IQueryable<Discussion> discussions= _unitOfWork.DiscussionRepository.GetManyQueryable(d => d.course_id == courseId);
            List<JObject> list = new List<JObject>();
            foreach (Discussion disc in discussions)
            {
                JObject o = new JObject();
                o["Id"] = disc.Id;
                o["Subject"] = disc.Subject;
                o["Description"] = disc.Description;
                o["Category"] = disc.Category;
                list.Add(o);
            }
            return list;
        }

        /*
            * GetUsersForCourse
            *
            * Get all users for a course
            *
            * @courseId (int) course id
            * @return (List<JObject>)
            */
        [Route("{courseId:int}/users")]
        [HttpGet]
        [CourseTeacherAuthorizationRequired]
        public List<JObject> getUsersForCourse(int courseId)
        {
            List<JObject> usersForCourse = new List<JObject>();
            IQueryable<Permission> usersWithPerm = _unitOfWork.PermissionRepository.GetManyQueryable(perm => perm.course_id == courseId && perm.Role.Equals("Student"));
            foreach (Permission perm in usersWithPerm)
            {
                JObject o = new JObject();
                o["Id"] = perm.User.Id;
                o["Username"] = perm.User.Username;
                o["First_name"] = perm.User.First_name;
                o["Last_name"] = perm.User.Last_name;
                usersForCourse.Add(o);
            }
            return usersForCourse;
        }


        /*
             * GetModulesForCourse
             *
             * Get all modules for course
             *
             * @courseId (int) course id
             * @return (ICollection<JObject>)
             */
        [Route("{courseId:int}/modules")]
        [HttpGet]
        [CoursePermissionRequired]
        public ICollection<JObject> GetModulesForCourse(int courseId)
        {
            Course course = _unitOfWork.CourseRepository.GetByID(courseId);
            IQueryable<Module> modules= _unitOfWork.ModuleRepository.GetManyQueryable(module => module.course_id == courseId);
            List<JObject> list = new List<JObject>();
            foreach (Module m in modules)
            {
                JObject o = new JObject();
                o["Id"] = m.Id;
                o["Name"] = m.Name;
                o["Start_date"] = m.Start_date;
                o["Content"] = m.Content;
                o["Description"] = m.Description;
                o["course_id"] = m.course_id;
                list.Add(o);
            }
            return list;
        }

        /*
            * GetPermissionsForCourse
            *
            * Get permissions for a course
            *
            * @courseId (int) course id
            * @return (ICollection<JObject>)
            */
        [Route("{courseId:int}/permissions")]
        [HttpGet]
        [AdminAuthorizationRequired]
        public ICollection<JObject> GetPermissionsForCourse(int courseId)
        {
            List<JObject> permissions = new List<JObject>();
            foreach (Permission myPermission in _unitOfWork.PermissionRepository.GetManyQueryable(perm => perm.course_id == courseId))
            {
                JObject o = new JObject();
                o["Id"] = myPermission.Id;
                o["Role"] = myPermission.Role;
                o["user_id"] = myPermission.user_id;
                o["course_id"] = myPermission.course_id;
                permissions.Add(o);
            }
            return permissions;
        }

        /*
             * GetAllAnnouncementsForCourse
             *
             * Get all announcements for a course
             *
             * @courseId (int) course id
             * @return (ICollection<JObject>)
             */
        [Route("{courseId:int}/announcements")]
        [HttpGet]
        [CoursePermissionRequired]
        public ICollection<JObject> GetAnnouncementsForCourse(int courseId)
        {
            Course course = _unitOfWork.CourseRepository.GetByID(courseId);
            IQueryable<Announcement> announces= _unitOfWork.AnnouncementRepository.GetManyQueryable(announce => announce.course_id == courseId);
            List<JObject> list = new List<JObject>();
            foreach (Announcement ann in announces)
            {
                JObject o = new JObject();
                o["Id"] = ann.Id;
                o["Title"] = ann.Title;
                o["Message"] = ann.Message;
                o["course_id"] = ann.course_id;
                list.Add(o);
            }
            return list;
        }

        //toate notele la un curs
        [Route("{courseId:int}/grades")]
        [HttpGet]
        [CoursePermissionRequired]
        [ResponseType(typeof(List<JObject>))]
        public IHttpActionResult GetGradesForCourse(int courseId)
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            List<JObject> myGrades = new List<JObject>();
            try { _unitOfWork.PermissionRepository.GetSingle(perm => perm.user_id == userId && perm.course_id == courseId && perm.Role.Equals("Student"));
            }
            catch (Exception)
            {
                return NotFound();
            }
                foreach (Module module in _unitOfWork.ModuleRepository.GetManyQueryable(obj => obj.course_id == courseId))
                {
                    foreach (Homework homework in _unitOfWork.HomeworkRepository.GetManyQueryable(obj => obj.module_id == module.Id))
                    {
                     try
                        {
                            Grade gradeH = _unitOfWork.GradeRepository.GetSingle(grade => grade.homework_id == homework.Id && grade.user_id == userId);
                            JObject o = new JObject();
                            o["Value"] = string.Format("{0:0.00}", gradeH.Value);
                            o["Name"] = homework.Name;
                            o["Module"] = module.Name;
                            myGrades.Add(o);
                        }
                        catch (Exception)
                        {
                        
                        if (homework.End_date < DateTime.Today)
                        {
                            JObject o = new JObject();
                            o["Value"] = 0;
                            o["Name"] = homework.Name;
                            o["Module"] = module.Name;
                            myGrades.Add(o);
                        }
                        }
                    }
                    foreach (Test test in _unitOfWork.TestRepository.GetManyQueryable(obj => obj.module_id == module.Id))
                    {                     
                        try
                        {
                            Grade gradeT = _unitOfWork.GradeRepository.GetSingle(grade => grade.test_id == test.Id && grade.user_id == userId);
                            JObject o = new JObject();
                            o["Value"] = string.Format("{0:0.00}", gradeT.Value);
                            o["Name"] = test.Name;
                            o["Module"] = module.Name;
                            myGrades.Add(o);
                        }
                        catch (Exception)
                        {
                        if (test.Due_date < DateTime.Today)
                        {
                            JObject o = new JObject();
                            o["Value"] = 0;
                            o["Name"] = test.Name;
                            o["Module"] = module.Name;
                            myGrades.Add(o);
                        }
                        }
                    }
                }
                foreach(Discussion discussion in _unitOfWork.DiscussionRepository.GetManyQueryable(obj => obj.course_id == courseId))
                {
                    try
                    {
                    Grade gradeD = _unitOfWork.GradeRepository.GetSingle(grade => grade.discussion_id==discussion.Id && grade.user_id == userId);
                        JObject o = new JObject();
                        o["Value"] = string.Format("{0:0.00}", gradeD.Value);
                        o["Name"] = discussion.Subject;
                        myGrades.Add(o);
                    }
                    catch (Exception)
                    {         }
                }
             
            return Ok(myGrades);
        }

        /*
            * GetMyFinalGrade
            *
            * Get my final grade for course id
            *
            * @courseId (int) course id
            * @return (float)
            */
        [Route("{courseId:int}/mygrade")]
        [HttpGet]
        [CourseStudentAuthozationRequired]
        public float GetMyFinalGrade(int courseId)
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            Course course = _unitOfWork.CourseRepository.GetByID(courseId);
            float finalGrade = 0;
            float courseTestGrade =(float)course.PondereTests* GetAllTestsGrade(courseId, userId);
            float courseHomeworkGrade = (float) course.PondereHomeworks*GetAllHomeworksGrade(courseId, userId);
            float courseDiscussionsGrade = (float)course.PondereDiscussions*GetAllDiscussionsGrade(courseId, userId);
            finalGrade = courseTestGrade + courseHomeworkGrade + courseDiscussionsGrade;
            // Grade grade = _unitOfWork.GradeRepository.Get(gr => gr.user_id == userId && gr.course_id == course.Id);
            Grade grade = db.Grades.SingleOrDefault(gr => gr.user_id == userId && gr.course_id == course.Id);
            if (grade==null){
                _unitOfWork.GradeRepository.Add(new Grade { Value = finalGrade, user_id = userId, course_id = courseId });
                _unitOfWork.Save();
            }
            else
            {
                grade.Value = finalGrade;
                grade.user_id = userId;
                grade.course_id = courseId;
                db.Entry(grade).State = EntityState.Modified;
                db.SaveChanges();
            }
            return finalGrade;
        }

        //returneaza media generala a userului la un curs
        [Route("{courseId:int}/checkGrade")]
        [HttpGet]
        [CourseStudentAuthozationRequired]
        public float CheckFinalGrade(int courseId)
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            Course course = _unitOfWork.CourseRepository.GetByID(courseId);
            Grade grade = db.Grades.SingleOrDefault(gr => gr.user_id == userId && gr.course_id == course.Id);
            if (grade == null)
                return 0;
            else
                return grade.Value;
        }

        /*
            * CalculateFinalGradeForUser
            *
            * Calculate final grade for user id and course id
            *
            * @courseId (int)  course id
            * @userId(int) user id
            * @return ()
            */
        public void CalculateFinalGradeForUser(int courseId,int userId)
        {
            Course course = _unitOfWork.CourseRepository.GetByID(courseId);//await db.Courses.FindAsync(courseId);
            float finalGrade = 0;
            float courseTestGrade = (float)course.PondereTests * GetAllTestsGrade(courseId, userId);
            float courseHomeworkGrade = (float)course.PondereHomeworks * GetAllHomeworksGrade(courseId, userId);
            float courseDiscussionsGrade = (float)course.PondereDiscussions * GetAllDiscussionsGrade(courseId, userId);
            finalGrade = courseTestGrade + courseHomeworkGrade + courseDiscussionsGrade;

            Grade grade = _unitOfWork.GradeRepository.Get(gr => gr.user_id == userId && gr.course_id == course.Id);
            if (grade == null)
            {
                //db.Grades.Add(new Grade { Value = finalGrade, user_id = userId, course_id = courseId });
                _unitOfWork.GradeRepository.Add(new Grade { Value = finalGrade, user_id = userId, course_id = courseId });
                _unitOfWork.Save();
            }
            else
            {
                grade.Value = finalGrade;
                grade.user_id = userId;
                grade.course_id = courseId;
                _unitOfWork.GradeRepository.Update(grade);
                _unitOfWork.Save();
            }
            UserNotification notif = new UserNotification { Seen = false, Message = "Media finala pentru cursul " + course.Name + " este " + finalGrade, user_id = userId };
            _unitOfWork.UserNotificationRepository.Add(notif);
            _unitOfWork.Save();
        }


        /*
            * CalculateFinalGrade
            *
            * Calculate the final grades for all users from a course id
            *
            * @courseId (int)  course id
            * @return (IHttpActionResult)
            */
        [Route("{courseId:int}/calculateFinals")]
        [HttpGet]
        [CourseTeacherAuthorizationRequired]
        public IHttpActionResult CalculateFinalGrades(int courseId)
        {
            //foreach(Permission perm in db.Permissions.Where(perm=>perm.course_id==courseId && perm.Role.Equals("Student")).ToList()){
            foreach (Permission perm in _unitOfWork.PermissionRepository.GetManyQueryable(perm => perm.course_id == courseId && perm.Role.Equals("Student")).ToList())//db.Permissions.Where(perm => perm.course_id == courseId && perm.Role.Equals("Student")).ToList())
                    CalculateFinalGradeForUser(courseId, (int)perm.user_id);
            return Ok();
        }

        [Route("{courseId}/groups")]
        [HttpGet]
        [CoursePermissionRequired]
        public ICollection<JObject> GetGroupsForCourse(int courseId)
        {
            IQueryable<Group> groups=_unitOfWork.GroupRepository.GetManyQueryable(group => group.course_id == courseId);
            List<JObject> list = new List<JObject>();
            foreach (Group gr in groups)
            {
                JObject o = new JObject();
                o["Id"] = gr.Id;
                o["Name"] = gr.Name;
                o["course_id"] = gr.course_id;
                list.Add(o);
            }
            return list;
        }

        /*
            * GetAllTestsGrade
            *
            * Get all test grades grade for a user
            *
            * @courseId (int) course id
            * @userId (int) user id
            * @return (float)
            */
        private float GetAllTestsGrade(int courseId, int userId)
        {
            if (userId == 0)
                userId = (int)ControllerContext.RouteData.Values["userId"];
            IQueryable<Module> CourseModules = _unitOfWork.ModuleRepository.GetManyQueryable(module => module.course_id == courseId);
            float testsGrade = 0;
            foreach (Module module in CourseModules)
            {
                IQueryable<Test> moduleTests = _unitOfWork.TestRepository.GetManyQueryable(test => test.module_id == module.Id);
                foreach (Test test in moduleTests)
                {
                    Grade testGrade = _unitOfWork.GradeRepository.Get(grade => grade.test_id == test.Id && grade.user_id == userId);
                    if (testGrade != null)
                        testsGrade += (float)test.Pondere * testGrade.Value;
                }
            }
            return testsGrade;
        }

        private float GetAllHomeworksGrade(int courseId, int userId)
        {
            IQueryable<Module> CourseModules = _unitOfWork.ModuleRepository.GetManyQueryable(module => module.course_id == courseId);
            float homeworksGrade = 0;
            foreach (Module module in CourseModules)
            {
                IQueryable<Homework> moduleHomeworks = _unitOfWork.HomeworkRepository.GetManyQueryable(homework => homework.module_id == module.Id);
                foreach (Homework homework in moduleHomeworks)
                {
                    Grade homeworkGrade = _unitOfWork.GradeRepository.Get(grade => grade.homework_id == homework.Id && grade.user_id == userId);
                    if (homeworkGrade != null)
                        homeworksGrade += (float)homework.Pondere * homeworkGrade.Value;
                }
            }
            return homeworksGrade;
        }

        private float GetAllDiscussionsGrade(int courseId, int userId)
        {
            float discussionsGrade = 0;
            IQueryable<Discussion> courseDiscussions = _unitOfWork.DiscussionRepository.GetManyQueryable(discussion => discussion.course_id == courseId);
            foreach (Discussion discussion in courseDiscussions)
            {               
                Grade discussionGrade = _unitOfWork.GradeRepository.Get(grade => grade.discussion_id == discussion.Id && grade.user_id == userId);
                    if (discussionGrade != null)
                     discussionsGrade += discussionGrade.Value;
            }
            discussionsGrade=discussionsGrade/courseDiscussions.Count();
            return discussionsGrade;
        }

        [Route("{courseId}/elements")]
        [HttpGet]
        [CourseTeacherAuthorizationRequired]
        public ICollection<JObject> GetElementsForCourse(int courseId)
        {
            IQueryable<Module> modules = _unitOfWork.ModuleRepository.GetManyQueryable(module => module.course_id == courseId);
            List<JObject> list = new List<JObject>();
            foreach (Module m in modules)
            {
                foreach (Test test in _unitOfWork.TestRepository.GetManyQueryable(obj => obj.module_id == m.Id))
                {
                    JObject o = new JObject();
                    o["Id"] = test.Id;
                    o["Name"] = test.Name;
                    o["Type"] = "Test";
                    list.Add(o);
                }

                foreach (Homework test in _unitOfWork.HomeworkRepository.GetManyQueryable(obj => obj.module_id == m.Id))
                {
                    JObject o = new JObject();
                    o["Id"] = test.Id;
                    o["Name"] = test.Name;
                    o["Type"] = "Tema";
                    list.Add(o);
                }
            }

            foreach (Discussion test in _unitOfWork.DiscussionRepository.GetManyQueryable(obj => obj.course_id==courseId))
            {
                JObject o = new JObject();
                o["Id"] = test.Id;
                o["Name"] = test.Subject;
                o["Type"] = "Discutie";
                list.Add(o);
            }
            return list;
        }
    }
}