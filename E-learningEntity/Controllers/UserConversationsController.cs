﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using E_learningEntity.Models;
using E_learningEntity.Services;
using E_learningEntity.ActionFilters;
using Newtonsoft.Json.Linq;

namespace E_learningEntity.Controllers
{
    [RoutePrefix("userConversations")]
    public class UserConversationsController : ApiController
    {
        private E_learningEntityContext db = new E_learningEntityContext();
        UnitOfWork _unitOfWork = new UnitOfWork();

        [Route("")]
        [HttpGet]
        [AuthorizationRequired]
        public ICollection<JObject> GetUserConversations()
        {
            int userId = (int)ControllerContext.RouteData.Values["userId"];
            List<JObject> list = new List<JObject>();
            IQueryable<UserConversation> userConv=_unitOfWork.UserConversationRepository.GetManyQueryable(conv => conv.user_id == userId);
            foreach (UserConversation myconv in userConv)
            {
                JObject o = new JObject();
                IQueryable<UserConversation> othersConv = _unitOfWork.UserConversationRepository.GetManyQueryable(conv => conv.conversation_id == myconv.conversation_id);
                JArray array = new JArray();
                foreach (UserConversation otherConv in othersConv)
                {
                    JObject ob = new JObject();
                    ob["Name"] = otherConv.User.Username;
                    ob["user_id"] = otherConv.user_id;
                    array.Add(ob);
                }
                o["Users"] = array;
                o["Id"] = myconv.conversation_id;
                o["Name"] = myconv.Conversation.Name;
                list.Add(o);            
            }
            return list;
        }

        [Route("{id:int}")]
        [HttpGet]
        [ResponseType(typeof(JObject))]
        public async Task<IHttpActionResult> GetUserConversation(int id)
        {
            UserConversation userConversation = await db.UserConversations.FindAsync(id);
            if (userConversation == null)
            {
                return NotFound();
            }
            JObject o = new JObject();
            o["Id"] = userConversation.Id;
            o["user_id"] = userConversation.user_id;
            o["conversation_id"] = userConversation.conversation_id;
            return Ok(o);
        }

        // PUT: api/UserConversations/5
        [ResponseType(typeof(void))]
        [Route("{id:int}")]
        [HttpPut]
        public async Task<IHttpActionResult> PutUserConversation(int id, UserConversation userConversation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userConversation.Id)
            {
                return BadRequest();
            }

            db.Entry(userConversation).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserConversationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [Route("")]
        [HttpPost]
        [ResponseType(typeof(JObject))]
        [AuthorizationRequired]
        public async Task<IHttpActionResult> PostUserConversation(UserConversation userConversation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.UserConversations.Add(userConversation);
            await db.SaveChangesAsync();
            JObject o = new JObject();
            o["Id"] = userConversation.Id;
            o["user_id"] = userConversation.user_id;
            o["conversation_id"] = userConversation.conversation_id;
            return Ok(o);
        }

        [Route("{id:int}")]
        [HttpDelete]
        [ResponseType(typeof(UserConversation))]
        public async Task<IHttpActionResult> DeleteUserConversation(int id)
        {
            UserConversation userConversation = await db.UserConversations.FindAsync(id);
            if (userConversation == null)
            {
                return NotFound();
            }

            db.UserConversations.Remove(userConversation);
            await db.SaveChangesAsync();

            return Ok(userConversation);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserConversationExists(int id)
        {
            return db.UserConversations.Count(e => e.Id == id) > 0;
        }

        //[Route("{convId:int}/messages")]
        //[HttpGet]
        //[AuthorizationRequired]
        //public ICollection<JObject> FindMessagesForUserConversation(int convId)
        //{
        //    int userId = (int)ControllerContext.RouteData.Values["userId"];
        //    List<Message> messages = new List<Message>();
        //    List<JObject> list = new List<JObject>();
        //    IQueryable<UserConversation> userConversation = _unitOfWork.UserConversationRepository.GetManyQueryable(conv => conv.conversation_id == convId);
        //    foreach(UserConversation userConv in userConversation)                
        //        messages.AddRange(_unitOfWork.MessageRepository.GetManyQueryable(message=>message.userConversation_id==userConv.Id));
        //    messages.OrderByDescending(i=>i.Date);
        //    foreach (Message m in messages)
        //    {
        //        JObject o=new JObject();
        //        o["Text"]=m.Text;
        //        o["Date"] = m.Date;
        //        o["Name"] = m.UserConversation.User.Last_name;
        //        list.Add(o);
        //    }
        //    return list;
        //}
    }
}