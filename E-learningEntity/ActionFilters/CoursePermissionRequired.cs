﻿using E_learningEntity.Services;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace E_learningEntity.ActionFilters
{
    public class CoursePermissionRequired: ActionFilterAttribute
    {
        private const string Token = "Token";
        public int courseId = 0;
        public int moduleId = 0;
        public UnitOfWork _unitOfWork = new UnitOfWork();

        public override void OnActionExecuting(HttpActionContext filterContext)
        {
                //  Get API key provider
                var provider = filterContext.ControllerContext.Configuration
                .DependencyResolver.GetService(typeof(ITokenServices)) as ITokenServices;

                if (filterContext.Request.Headers.Contains(Token))
                {
                    var tokenValue = filterContext.Request.Headers.GetValues(Token).First();
                    try { //Ia courseId din controller
                        courseId = (int)filterContext.ActionArguments["courseId"];
                    }
                    catch (Exception) {
                          moduleId = (int)filterContext.ActionArguments["moduleId"];
                }

                if (moduleId != 0 && courseId == 0)
                {
                    courseId = _unitOfWork.ModuleRepository.GetByID(moduleId).course_id;
                }
                else
                    if (courseId != 0)
                {
                    if (provider == null || !provider.ValidateToken(tokenValue) || !provider.ValidateCoursePermissions(tokenValue, courseId))
                    {
                        var responseMessage = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = "Invalid Request" };
                        filterContext.Response = responseMessage;
                    }
                    else
                        filterContext.ControllerContext.RouteData.Values.Add("userId", provider.GetUserIdByToken(tokenValue));
                }

            }
                else
                {
                    filterContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                }
                base.OnActionExecuting(filterContext);
            }
        }
    }
