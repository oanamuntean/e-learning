﻿using E_learningEntity.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace E_learningEntity.ActionFilters
{
    public class AuthorizationRequired : ActionFilterAttribute
    {
        private const string Token = "Token";
       // public int userId=0;
        public HttpResponseMessage ResponseMessage;

        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            //  Get API key provider
            var provider = filterContext.ControllerContext.Configuration
            .DependencyResolver.GetService(typeof(ITokenServices)) as ITokenServices;

            if (filterContext.Request.Headers.Contains(Token))
            {
                var tokenValue = filterContext.Request.Headers.GetValues(Token).First();
               
                // Validate Token
                if (provider != null && !provider.ValidateToken(tokenValue))
                {
                    var responseMessage = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = "Invalid Request" };
                    filterContext.Response = responseMessage;
                }
                else
                {
                    if(!filterContext.ControllerContext.RouteData.Values.ContainsKey("userId"))
                    filterContext.ControllerContext.RouteData.Values.Add("userId", provider.GetUserIdByToken(tokenValue));
                }                
            }
            else
            {
                filterContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            }

            base.OnActionExecuting(filterContext);
            ResponseMessage = filterContext.Response;
        }
    }
}