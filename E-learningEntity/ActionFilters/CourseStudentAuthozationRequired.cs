﻿using E_learningEntity.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace E_learningEntity.ActionFilters
{
    //validare dupa courseId =>returneaza si userId
    public class CourseStudentAuthozationRequired : ActionFilterAttribute
    {
        private const string Token = "Token";
        int courseId = 0;

        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            //  Get API key provider
            var provider = filterContext.ControllerContext.Configuration
            .DependencyResolver.GetService(typeof(ITokenServices)) as ITokenServices;

            if (filterContext.Request.Headers.Contains(Token))
            {
                var tokenValue = filterContext.Request.Headers.GetValues(Token).First();
                courseId = (int)filterContext.ActionArguments["courseId"];

                // Validate Token
                     if (provider == null || !provider.ValidateToken(tokenValue) || !provider.ValidateTokenStudentForCourse(tokenValue, courseId))
                    {
                        var responseMessage = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = "Invalid Request" };
                        filterContext.Response = responseMessage;
                    }
                     else
                     { //daca e valid, pune userId in RouteData
                         if (!filterContext.ControllerContext.RouteData.Values.ContainsKey("userId"))
                         filterContext.ControllerContext.RouteData.Values.Add("userId", provider.GetUserIdByToken(tokenValue));
                     } 
               }
            else
            {
                filterContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            }
            base.OnActionExecuting(filterContext);
        }
    }
}